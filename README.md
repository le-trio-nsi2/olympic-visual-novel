# Olympic Visual Novel


# Description du Projet

Ce projet est une simulation de jeu de type visual novel développé en utilisant la bibliothèque Pygame en Python.

## Principales Fonctionnalités et Composants

- **Initialisation de Pygame :** Pygame est initialisé pour exploiter ses fonctionnalités de rendu graphique et de gestion des événements.

- **Définition des constantes :** Définition des constantes pour la largeur et la hauteur de la fenêtre de jeu, ainsi que les couleurs utilisées.

- **Initialisation de la fenêtre de jeu :** Création d'une fenêtre avec les dimensions spécifiées. Un titre est également défini.

- **Chargement et redimensionnement des images :** Chargement de deux images depuis des fichiers et leur redimensionnement pour s'adapter à la fenêtre de jeu.

- **Fonctions d'affichage du texte et des images :** Fonctions dédiées à l'affichage de texte et d'images chargées sur l'écran.

- **Définition des dialogues :** Liste de dialogues sous forme de paires, chaque dialogue ayant un texte et une liste d'options de réponse.

- **Boucle principale :** Gestion des événements Pygame, dessin des éléments du jeu, obtention des choix du joueur, et mise à jour des dialogues selon ces choix.

Ce projet offre une expérience de visual novel où le joueur influence le déroulement de l'histoire en sélectionnant des options de dialogue.