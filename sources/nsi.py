import pygame
import sys

# Initialisation de Pygame
pygame.init()

# Définition des constantes
LARGEUR = 800
HAUTEUR = 600

# Couleurs
BLANC = (255, 255, 255)
NOIR = (0, 0, 0)

# Initialisation de la fenêtre de jeu
ecran = pygame.display.set_mode((LARGEUR, HAUTEUR))
pygame.display.set_caption("Visual Novel")

# Chargement des images
fond_image = pygame.image.load("ai-generated-8090592_1920.jpg")
personnage_image = pygame.image.load("ai-generated-8244632_1280.png")

# Redimensionnement des images pour les adapter à la fenêtre
fond_image = pygame.transform.scale(fond_image, (LARGEUR, HAUTEUR))
personnage_image = pygame.transform.scale(personnage_image, (200, 400))

# Fonction pour afficher le texte
def afficher_texte(texte, position_y):
    font = pygame.font.Font(None, 24)
    texte_surface = font.render(texte, True, NOIR)
    texte_rect = texte_surface.get_rect(center=(LARGEUR/2, position_y))
    ecran.blit(texte_surface, texte_rect)


# Définition des dialogues
dialogues = [
    ("Jane: Salut, bienvenue aux Jeux Olympiques ! Que veux-tu dire?", ["Parler de la météo.", "Demander sur le sport."], [1, 2]),
    ("Jackie: Merci ! C'est un honneur d'être ici. Que préfères-tu?", ["Parler de l'entraînement.", "Demander sur le dopage."], [3, 4]),
    ("Jane: Tu participes à quel sport?", ["Natation.", "Tennis."], [5, 6]),
    ("Jackie: J'ai entendu dire que le tennis peut être difficile. Tu t'entraînes beaucoup?", ["Oui, j'essaie de m'entraîner tous les jours.", "Pas vraiment, je préfère me détendre."], [7, 8]),
    ("Jane: As-tu entendu parler des epreuves des Jeux Olympiques de cette année?", ["Oui, j'ai entendu parler des jeux olympiques.", "Non, je ne suis pas au courant."], [9, 10]),
    ("Jane: Bien sûr. Les Jeux de cette année soutiennent divers programmes de développement communautaire, tels que la construction d'infrastructures sportives accessibles à tous, la promotion de modes de vie sains et la revitalisation des quartiers locaux.", ["1. C'est formidable.", "2. Y a-t-il des projets spécifiques qui vous intéressent?"],[11, 12]),
    ("Jackie: C'est formidable de voir comment les Jeux contribuent au développement des communautés locales. Y a-t-il des projets spécifiques qui vous intéressent?", ["1. Parler de l'initiative de recyclage des équipements sportifs.", "2. Demander sur le programme de mentorat pour les jeunes."],[13, 14]),
]

# Fonction pour afficher les images
def afficher_images():
    ecran.blit(fond_image, (0, 0))
    ecran.blit(personnage_image, (100, 100))

# Fonction pour obtenir le choix du joueur
def obtenir_choix():
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_1:
                    return 1
                elif event.key == pygame.K_2:
                    return 2

# Boucle principale
clock = pygame.time.Clock()
FPS = 30  # Nombre de frames par seconde

def main():
    running = True
    dialogue_index = 0
    score_joueur = 0  # Initialisation du score du joueur

    while running:
        clock.tick(FPS)  # Limiter la vitesse de la boucle principale
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False

        ecran.fill(BLANC)
        afficher_images()

        if 0 <= dialogue_index < len(dialogues):
            afficher_texte(dialogues[dialogue_index][0], 60)
            afficher_texte("Choisissez votre action :", 100)
            for i, choix in enumerate(dialogues[dialogue_index][1], start=1):
                afficher_texte(f"{i}. {choix}", 150 + i * 30)

            pygame.display.flip()

            choix = obtenir_choix()

            if choix == 1:
                if dialogue_index < len(dialogues) - 1 and len(dialogues[dialogue_index][2]) > 0:
                    dialogue_index = dialogues[dialogue_index][2][0]
            elif choix == 2:
                if dialogue_index < len(dialogues) - 1 and len(dialogues[dialogue_index][2]) > 1:
                    dialogue_index = dialogues[dialogue_index][2][1]
        else:
            running = False

    pygame.quit()
    sys.exit()

# Appel de la fonction main
main()
